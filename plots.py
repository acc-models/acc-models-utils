import re
import pandas as pd
import numpy as np
import tfs   # pip install tfs-pandas

from bokeh.plotting import figure, output_file, output_notebook, show, save, ColumnDataSource
from bokeh.models import Legend, LinearAxis, Range1d, CustomJS, Slider, Span, Panel, Tabs
from bokeh.models.glyphs import Rect
from bokeh.layouts import row, column, gridplot
import bokeh.palettes

class plots:

    def betarel(gamma):
        return np.sqrt(1-1/gamma**2)

    def plot_optics(twiss, filename, closed_orbit = False, output = 'file', labelsFont='times', labelsFontSize='12pt'):
        # definition of parameters to be shown when hovering the mouse over the data points

        # Load TWISS table as dataframe
        if type(twiss) == str:
            if twiss[-3:] == 'tfs':
                twiss = tfs.read(twiss)
            elif twiss[-3:] == 'pkl':
                twiss = pd.read_pickle(twiss)
        elif type(twiss) == tfs.handler.TfsDataFrame:
            pass
        else: 
            raise TypeError("Twiss input is expected to be a string (either '*.tfs' for a MAD-X TFS table or '*.pkl' for a pickled tfs-pandas TfsDataFrame) or directly a TfsDataFrame.")

        # Extract Twiss Header
        twissHeader = dict(twiss.headers)

        # Prepare output
        if output == 'file':
            # output to HTML
            output_file(filename, mode="inline")
        elif output == 'inline':
            # embedded plot in a Jupyter notebook
            output_notebook()

        tooltips = [("parameter", "$name"), ("element", "@name"), ("value [m]", "$y")]
        tooltips_mm = [("parameter", "$name"), ("element", "@name"), ("value [mm]", "$y")]
        tooltips_elements = [("element", "@name")]

        # define the datasource
        data = pd.DataFrame(columns = ['s', 'name', 'key', 'length', u"\u03B2x", u"\u03B2y", "Dx", "Dy", "x", "y"])
        data['s']      = twiss.S
        data['name']   = twiss.NAME
        data['key']    = twiss.KEYWORD
        data['length'] = twiss.L
        data['x']      = twiss.X * 1e3
        try:
            data['y']      = twiss.Y * 1e3
        except AttributeError:
            data['y']      = twiss.X * 0.

        # Betas and Dispersion is different if PTC or TWISS
        if (('TYPE' in twissHeader) and (twissHeader['TYPE'] == 'PTC_TWISS')) or ('NAME' in twissHeader) and (twissHeader['NAME'] == 'PTC_TWISS'):
            # PTC TWISS
            data[u"\u03B2x"] = twiss.BETA11
            data[u"\u03B2y"] = twiss.BETA22
            data['Dx']       = twiss.DISP1
            try:
                data['Dy']       = twiss.DISP3
            except AttributeError:
                data['Dy']       = twiss.DISP1 * 0.0
        else:
            # MAD-X TWISS
            data[u"\u03B2x"] = twiss.BETX 
            data[u"\u03B2y"] = twiss.BETY
            data['Dx']       = twiss.DX * plots.betarel(twissHeader['GAMMA'])
            try:
                data['Dy']       = twiss.DY * plots.betarel(twissHeader['GAMMA'])
            except AttributeError:
                data['Dy']       = twiss.DX * 0.0

        # calculate plot limits based on data range
        # beta-functions:
        bmin = (np.floor(np.min([data[u"\u03B2x"], data[u"\u03B2y"]])/5)) * 5
        bmax = (np.floor(np.max([data[u"\u03B2x"], data[u"\u03B2y"]])/5) + 1) * 5  
        b_p2p = bmax - bmin
        betaPlotRange = Range1d(start = bmin - b_p2p/2, end = bmax)

        # dispersion-function:
        dxmin = (np.floor(np.min([data['Dx'].min(), data['Dy'].min()]) * 12)/10)
        dxmax = (np.floor(np.max([data['Dx'].max(), data['Dy'].max()]) * 12)/10)
        dx_p2p = dxmax - dxmin
        dispPlotRange = Range1d(start = dxmin, end = dxmax + dx_p2p * 2)

        # create a new plot with a title and axis labels
        f = figure(title="", x_axis_label='s [m]', y_axis_label=u'\u03B2-functions [m]', width=1000, height=500, 
                    x_range=Range1d(0, twiss.LENGTH, bounds="auto"), y_range=betaPlotRange, tools="box_zoom, pan, reset, hover", 
                    active_drag = 'box_zoom', tooltips = tooltips)
        #f.xaxis.axis_label_text_font_size = labelsFontSize
        #f.yaxis.axis_label_text_font_size = labelsFontSize
        f.axis.major_label_text_font = labelsFont
        f.axis.major_label_text_font_size = labelsFontSize
        f.axis.axis_label_text_font = labelsFont
        f.axis.axis_label_text_font_size = labelsFontSize
        f.axis.axis_label_text_font_style = 'normal'
        f.outline_line_color = 'black'
        #f.sizing_mode = 'scale_width'

        # add beta function lines:
        source = ColumnDataSource(data)
        cols = ['darkblue', 'salmon', 'green', 'black']
        for j, col in enumerate([u"\u03B2x", u"\u03B2y"]):
            f.line('s', col, source=source, name = col, line_width=1.5, line_color = cols[j])

        # prepare legend items
        legendItems = [(u"\u03B2x", [f.renderers[0]]), (u"\u03B2y", [f.renderers[1]])]

        # Adding section number for PS ring
        # TODO: should probably be removed if to use it as generic tool
        if twissHeader['SEQUENCE'] == 'PS':
            # define tick locations for each SS
            SS_ticks = twiss[['NAME', 'S']]

            SS_ticks.drop_duplicates(subset='NAME', keep = 'last', inplace=True, ignore_index=True)
            SS_ticks = SS_ticks[SS_ticks['NAME'].str.startswith('PS') & SS_ticks['NAME'].str.endswith('START')]
            SS_ticks['SS'] = SS_ticks['NAME'].apply(lambda x: x[2:4])
            SS_ticks.set_index(SS_ticks['S'], inplace=True)

            # Setting the range name and range for top x-axis
            f.extra_x_ranges = {"sections": Range1d(start=0.0001, end=2*np.pi*100, bounds="auto")}

            # Adding the second x axis to the plot.  
            f.add_layout(LinearAxis(x_range_name="sections", axis_label='Start of straight section', 
                    axis_label_text_font = labelsFont, axis_label_text_font_style = 'normal', 
                    axis_label_text_font_size = labelsFontSize, major_label_text_font = labelsFont, major_label_text_font_size = labelsFontSize), 'above')
            tick_pos = SS_ticks['S'].values
            try:
                tick_pos[0] = 0.0001
            except:
                print('Some bug here...')
            f.xaxis[0].ticker = tick_pos
            f.xaxis[0].major_label_overrides = SS_ticks.to_dict()['SS']

        # Adding the second y axis to the plot.  
        if (np.max(np.abs(data['Dx'])) > 0.) or (np.max(np.abs(data['Dy'])) > 0.) :
            # Setting the second y axis range name and range
            f.extra_y_ranges = {"disp": dispPlotRange}
            f.extra_x_ranges = {"sections": Range1d(start=0.0001, end=twiss.S.max(), bounds="auto")}

            f.add_layout(LinearAxis(y_range_name="disp", axis_label='D [m]', axis_label_text_font = labelsFont, 
                                axis_label_text_font_style = 'normal', axis_label_text_font_size = labelsFontSize,
                                major_label_text_font = labelsFont, major_label_text_font_size = labelsFontSize),
                                'right')
            if (np.max(np.abs(data['Dx'])) > 0.):
                dx = f.line('s', 'Dx', source=source, name = 'Dx', line_width=1.5, line_color = 'black',  y_range_name='disp')
                legendItems.append(("Dx", [dx]))
            if (np.max(np.abs(data['Dy'])) > 0.):    
                dy = f.line('s', 'Dy', source=source, name = 'Dy', line_width=1.5, line_color = 'orange', y_range_name='disp')
                legendItems.append(("Dy", [dy]))        
        
        # finalize figure with beta and dispersion functions by adding legend
        legend = Legend(items=legendItems, location=(10, 165)) 
        f.add_layout(legend, 'right')
        legend.label_text_font = labelsFont
        legend.label_text_font_size = labelsFontSize

        #--------------------------------------------------------
        # create figure for lattice elements
        f0 = figure(title="", width=1000, height=40, x_range=f.x_range, y_range=(-1.25, 1.25), 
                    tools="box_zoom, pan, reset, hover", active_drag = 'box_zoom', tooltips = tooltips_elements)
        f0.axis.visible = False
        f0.grid.visible = False
        f0.outline_line_color = 'white'
        f0.sizing_mode = 'scale_width'
        f0.toolbar.logo = None
        f0.toolbar_location = None
        # Add plots of elements
        twiss.drop_duplicates(subset='NAME', keep = 'last', inplace=True, ignore_index=True)
        plots.plot_lattice_elements(f0, twiss, filename)

        #--------------------------------------------------------
        # add additional figure for orbit plot
        if closed_orbit == True:
            # define axis limits of closed orbit plot based on maximum excursion
            xmin = (np.floor(np.min([data['x'].min(), data['y'].min()]) * 12)/10)-1
            xmax = (np.floor(np.max([data['x'].max(), data['y'].max()]) * 12)/10)+1
            f1 = figure(title="", x_axis_label='s [m]', y_axis_label='x, y [mm]', width=1000, height=150, 
                    x_range=f.x_range, 
                    y_range=(xmin, xmax),
                    tools="box_zoom, pan, reset, hover", active_drag = 'box_zoom', tooltips = tooltips_mm)
            
            f1.axis.major_label_text_font = labelsFont
            f1.axis.major_label_text_font_size = labelsFontSize
            f1.axis.axis_label_text_font = labelsFont
            f1.axis.axis_label_text_font_size = labelsFontSize
            f1.axis.axis_label_text_font_style = 'normal'
            f1.outline_line_color = 'black'
            #f1.sizing_mode = 'scale_width'

            x=f1.line('s', 'x', source=source, name = 'x', line_width=1.5, line_color = 'black')
            y=f1.line('s', 'y', source=source, name = 'y', line_width=1.5, line_color = 'salmon')

            f1.toolbar.logo = None
            f1.toolbar_location = None
            
            legend = Legend(items=[('x', [x]),('y', [y])]) 
            f1.add_layout(legend, 'right')
            legend.label_text_font = labelsFont
            legend.label_text_font_size = labelsFontSize

            if output == 'file':
                # save the results for standard configurations with bump
                save(column([f0, f, f1], sizing_mode = 'scale_width'))
            elif output == 'inline':
                show(column([f0, f, f1], sizing_mode = 'scale_width'))
        else:
            # save the results for standard configurations without closed orbit
            if output == 'file':
                save(column([f0, f]))
            elif output == 'inline':
                show(column([f0, f]))

    def plot_MTE_optics(twiss, filename, closed_orbit = False, output = 'file'):
        # definition of parameters to be shown when hovering the mouse over the data points

        if type(twiss) == str:
            if twiss[-3:] == 'tfs':
                twiss = tfs.read(twiss)
            elif twiss[-3:] == 'pkl':
                twiss = pd.read_pickle(twiss)
        elif type(twiss) == tfs.handler.TfsDataFrame:
            pass
        else: 
            raise TypeError("Twiss input is expected to be a string (either '*.tfs' for a MAD-X TFS table or '*.pkl' for a pickled tfs-pandas TfsDataFrame) or directly a TfsDataFrame.")

        if output == 'file':
            # output to HTML
            output_file(filename, mode="inline")
        elif output == 'inline':
            # embedded plot in a Jupyter notebook
            output_notebook()

        tooltips = [("parameter", "$name"), ("element", "@name"), ("value [m]", "$y")]
        tooltips_elements = [("element", "@name")]

        # define the datasource
        data = pd.DataFrame(columns = ['s', u"\u03B2x", u"\u03B2y", "Dx"])
        data['s'] = twiss.S
        data[u"\u03B2x"] = twiss.BETA11
        data[u"\u03B2y"] = twiss.BETA22
        data['Dx'] = twiss.DISP1
        data['x'] = twiss.X * 1e3

        try:
            data['y'] = twiss.Y * 1e3
        except AttributeError:
            data['y'] = twiss.X * 0.

        data['name'] = twiss.NAME
        data['key'] = twiss.KEYWORD
        data['length'] = twiss.L

        beamlets = ['core']
        tab = []

        # define data source for the MTE islands 
        if len(tfs_file) > 1:
            beamlets = ['core', 'island 1', 'island 2', 'island 3', 'island 4']
            twiss_island = pd.read_pickle(tfs_file[1])

        for i, beamlet in enumerate(beamlets):
            #redefine data source for the islands
            if i == 1:
                #---------------------------------------------------------
                # define the datasource for the islands
                data = pd.DataFrame(columns = ['s', u"\u03B2x", u"\u03B2y", "Dx"])
                data['s'] = twiss_island.S
                data[u"\u03B2x"] = twiss_island.BETA11
                data[u"\u03B2y"] = twiss_island.BETA22
                data['Dx'] = twiss_island.DISP1
                data['x'] = twiss_island.X * 1e3
                data['name'] = twiss_island.NAME 
                #---------------------------------------------------------

            source = ColumnDataSource(data)

            # calculate plot limits based on data range
            # beta-functions:
            bmin = (np.floor(np.min([data[u"\u03B2x"], data[u"\u03B2y"]])/5)) * 5
            bmax = (np.floor(np.max([data[u"\u03B2x"], data[u"\u03B2y"]])/5) + 1) * 5  
            b_p2p = bmax - bmin
            y = Range1d(start = bmin - b_p2p/2, end = bmax)

            # dispersion-function:
            if 'ps_' in filename:
                dxmin = (np.floor(np.min(data['Dx'])))
                dxmax = (np.floor(np.max(data['Dx'])) + 0.5)  
                dx_p2p = dxmax - dxmin
                dx = Range1d(start = dxmin, end = dxmax + dx_p2p*2)
            elif ('psb_' in filename) or ('leir_' in filename):
                dxmin = (np.floor(np.max(data['Dx'])*10)/10)
                dxmax = (np.floor(np.min(data['Dx'])*10)/10)  
                dx_p2p = dxmax - dxmin
                dx = Range1d(start = dxmax, end = dxmin - dx_p2p*2)

            # create a new plot with a title and axis labels
            f = figure(title="", x_axis_label='s [m]', y_axis_label=u'\u03B2-functions [m]', width=1000, height=500, x_range=Range1d(0, twiss.LENGTH, bounds="auto"), y_range=y, tools="box_zoom, pan, reset, hover", active_drag = 'box_zoom', tooltips = tooltips)

            f.axis.major_label_text_font = 'times'
            f.axis.axis_label_text_font = 'times'
            f.axis.axis_label_text_font_style = 'normal'
            f.outline_line_color = 'black'
            f.sizing_mode = 'scale_width'

            # shift data to plot the different islands in the different tabs 
            if i > 0:
                source.data['s'] = source.data['s'] - 2 * np.pi *100 * (i-1)

            cols = ['darkblue', 'salmon']

            for j, col in enumerate(data.columns[1:3]):
                f.line('s', col, source=source, name = col, line_width=1.5, line_color = cols[j])

            # Setting the second y axis range name and range
            f.extra_y_ranges = {"disp": dx}

            if 'ps_' in filename:
                # load tick locations for each SS
                SS_ticks = pd.read_pickle('./_scripts/web/section_limits/PS_SS_limits.pkl')

                # Setting the range name and range for top x-axis
                f.extra_x_ranges = {"sections": Range1d(start=0.0001, end=2*np.pi*100, bounds="auto")}

                # Adding the second x axis to the plot.  
                f.add_layout(LinearAxis(x_range_name="sections", axis_label='Start of straight section', axis_label_text_font = 'times', axis_label_text_font_style = 'normal', major_label_text_font = 'times'), 'above')
                tick_pos = SS_ticks.index.values
                tick_pos[0] = 0.0001
                f.xaxis[0].ticker = tick_pos
                f.xaxis[0].major_label_overrides = SS_ticks.to_dict()['SS']

            # Adding the second y axis to the plot.  
            f.add_layout(LinearAxis(y_range_name="disp", axis_label='Dx [m]', axis_label_text_font = 'times', axis_label_text_font_style = 'normal', major_label_text_font = 'times'), 'right')
            dx = f.line('s', 'Dx', source=source, name = 'Dx', line_width=1.5, line_color = 'black', y_range_name="disp")

            legend = Legend(items=[(u"\u03B2x", [f.renderers[0]]), (u"\u03B2y", [f.renderers[1]]), ("Dx", [f.renderers[2]])], location=(10, 165))
            f.add_layout(legend, 'right')
            legend.label_text_font = 'times'

            tooltips = [("parameter", "$name"), ("element", "@name"), ("value [mm]", "$y")]

            # define limits of x-plot based on maximum excursion of the islands
            if len(beamlets) > 1:
                y = (np.floor(min(twiss_island.X*1e3)/10) * 10, (np.floor(max(twiss_island.X*1e3)/10) + 1) * 10)
            else:
                y = (np.floor(min(data['x'])/10) * 10, (np.floor(max(data['x'])/10) + 1) * 10)

            #--------------------------------------------------------
            # add additional axis to plot elements

            twiss.drop_duplicates(subset='NAME', keep = 'last', inplace=True)

            f0 = figure(title="", width=1000, height=40, x_range=f.x_range, y_range=(-1.25, 1.25), tools="box_zoom, pan, reset, hover", active_drag = 'box_zoom', tooltips = tooltips_elements)

            f0.axis.visible = False
            f0.grid.visible = False
            f0.outline_line_color = 'white'
            f0.sizing_mode = 'scale_width'

            f0.toolbar.logo = None
            f0.toolbar_location = None

            plots.plot_lattice_elements(f0, twiss, filename)

            #--------------------------------------------------------
            # add additional axis for orbit plot
            f1 = figure(title="", x_axis_label='s [m]', y_axis_label='x [mm]', width=1000, height=150, x_range=f.x_range, y_range=y, tools="box_zoom, pan, reset, hover", active_drag = 'box_zoom', tooltips = tooltips)

            f1.axis.major_label_text_font = 'times'
            f1.axis.axis_label_text_font = 'times'
            f1.axis.axis_label_text_font_style = 'normal'
            f1.outline_line_color = 'black'
            f1.sizing_mode = 'scale_width'

            if 'leir_' in filename:
                f1.line('s', 'x', source=source, name = 'x', line_width=1.5, line_color = 'black', legend = ' x')
                f1.line('s', 'y', source=source, name = 'y', line_width=1.5, line_color = 'salmon', legend = ' y')
                f1.yaxis.axis_label = 'x, y [mm]' 
            else:
                f1.line('s', 'x', source=source, name = 'x', line_width=1.5, line_color = 'black')
                f1.yaxis.axis_label = 'x [mm]' 

            f1.toolbar.logo = None
            f1.toolbar_location = None

            tab.append(Panel(child = column([f0, f, f1], sizing_mode = 'scale_width'), title = beamlet))

        if len(tab) > 1:
            tabs = Tabs(tabs=[ t for t in tab ])
            if output == 'file':
                # save the results for MTE configurations
                save(tabs)
            elif output == 'inline':
                show(tabs)
        else:
            if (('ps_ext_' in filename) or ('_inj_' in filename) or ('ps_se_' in filename) or ('leir_' in filename)) and not ('ps_ext_sftpro' in filename):
                if output == 'file':
                    # save the results for standard configurations with bump
                    save(column([f0, f, f1], sizing_mode = 'scale_width'))
                elif output == 'inline':
                    show(column([f0, f, f1], sizing_mode = 'scale_width'))
            else:
                # save the results for standard configurations without bump
                if output == 'file':
                    save(column([f0, f]))
                elif output == 'inline':
                    show(column([f0, f]))

    def create_Columndatasource(parameters, values):
        # parameters and data have to be a list

        data = pd.DataFrame(columns = parameters)
        for i, p in enumerate(parameters):
            data[p] = values[i]

        return ColumnDataSource(data)

    def plot_lattice_elements(figure, twiss, filename):

        # Extract Twiss Header
        twissHeader = dict(twiss.headers)

        pos = twiss.S.values - twiss.L.values/2
        lengths = twiss.L.values
        # modify lengths in order to plot zero-length elements
        lengths[np.where(lengths == 0)[0]] += 0.001

        # BENDS    
        idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD) if ('BEND' in elem) or ('MATRIX' in elem)])
        idx_0 = idx[twiss.K1L[idx] == 0]
        # distinguish F and D units 
        idx_1 = idx[twiss.K1L[idx] > 0]
        idx_2 = idx[twiss.K1L[idx] < 0]
        cols = ['#2ca25f', bokeh.palettes.Reds8[2], bokeh.palettes.Blues8[1]]
        for i, indx in enumerate([idx_0, idx_1, idx_2]):
            if len(indx) > 0:
                source = plots.create_Columndatasource(['pos', 'width', 'name'], [pos[indx], lengths[indx], np.array(twiss.NAME)[indx]])
                figure.rect(x = 'pos', y = 0, width = 'width', height = 2, fill_color=cols[i], line_color = 'black', source = source)


        # QUADRUPOLES
        idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD) if 'QUADRUPOLE' in elem])
        
        if twissHeader['SEQUENCE'] == 'PS':
            name = np.array(twiss.NAME.values)[idx]
            # loc = [map(int, re.findall(r'\d+', n))[-1]%2 for n in name] # extract SS information from string and check whether it is even or odd SS
            loc = [int(re.findall(r'\d+', n)[-1])%2 for n in name]
            idx_1 = idx[np.array(loc) == 1]     # even SS - focusing
            idx_2 = idx[np.array(loc) == 0]     # odd SS - defocusing
        else:
            idx_1 = idx[twiss.K1L[idx] > 0]
            idx_2 = idx[twiss.K1L[idx] < 0]

        cols = [bokeh.palettes.Reds8[2], bokeh.palettes.Blues8[1]]
        offset = [0.6, -0.6]
        for i, indx in enumerate([idx_1, idx_2]):
            if len(indx) > 0:
                source = plots.create_Columndatasource(['pos', 'width', 'name'], [pos[indx], lengths[indx], np.array(twiss.NAME)[indx]])
                figure.rect(x = 'pos', y = 0 + offset[i], width = 'width', height = 1.2, fill_color=cols[i], line_color = 'black', source = source)

        # SEXTUPOLES
        idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD) if 'SEXTUPOLE' in elem])
        try:
            if twissHeader['SEQUENCE'] == 'PS':
                name = np.array(twiss.NAME.values)[idx]
                # loc = [map(int, re.findall(r'\d+', n))[-1]%2 for n in name] # extract SS information from string and check whether it is even or odd SS
                loc = [int(re.findall(r'\d+', n)[-1])%2 for n in name]
                idx_1 = idx[np.array(loc) == 1]     # even SS - focusing
                idx_2 = idx[np.array(loc) == 0]     # odd SS - defocusing 
            else:
                idx_1 = idx[twiss.K2L[idx] > 0]
                idx_2 = idx[twiss.K2L[idx] < 0]       
        except IndexError:
            idx_1 = idx
            idx_2 = []
            pass
        offset = [0.4, -0.4]
        for i, indx in enumerate([idx_1, idx_2]):
            if len(indx) > 0:
                source = plots.create_Columndatasource(['pos', 'width', 'name'], [pos[indx], lengths[indx], np.array(twiss.NAME)[indx]])
                figure.rect(x = 'pos', y = 0 + offset[i], width = 'width', height = 0.8, fill_color='#fff7bc', line_color = 'black', source = source)
            
        # OCTUPOLES
        idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD) if 'OCTUPOLE' in elem])
        try:
            if twissHeader['SEQUENCE'] == 'PS':
                name = np.array(twiss.NAME.values)[idx]
                # loc = [map(int, re.findall(r'\d+', n))[-1]%2 for n in name] # extract SS information from string and check whether it is even or odd SS
                loc = [int(re.findall(r'\d+', n)[-1])%2 for n in name]
                idx_1 = idx[np.array(loc) == 1]     # even SS - focusing
                idx_2 = idx[np.array(loc) == 0]     # odd SS - defocusing  
            else:
                idx_1 = idx[twiss.K3L[idx] > 0]
                idx_2 = idx[twiss.K3L[idx] < 0]         
        except IndexError:
            idx_1 = idx
            idx_2 = []
            pass       
        offset = [0.3, -0.3]
        for i, indx in enumerate([idx_1, idx_2]):
            if len(indx) > 0:
                source = plots.create_Columndatasource(['pos', 'width', 'name'], [pos[indx], lengths[indx], np.array(twiss.NAME)[indx]])
                figure.rect(x = 'pos', y = 0 + offset[i], width = 'width', height = 0.6, fill_color='#756bb1', line_color = 'black', source = source)

        # KICKERS
        idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD) if 'KICKER' in elem])
        if len(idx) > 0:
            source = plots.create_Columndatasource(['pos', 'width', 'name'], [pos[idx], lengths[idx], np.array(twiss.NAME)[idx]])
            figure.rect(x = 'pos', y = 0, width = 'width', height = 2, fill_color='#1b9e77', line_color = 'black', source = source)

        # CAVITIES
        idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD) if 'CAVITY' in elem])
        if len(idx) > 0:
            source = plots.create_Columndatasource(['pos', 'width', 'name'], [pos[idx], lengths[idx], np.array(twiss.NAME)[idx]])
            figure.rect(x = 'pos', y =  0.7, width = 'width', height = 1, fill_color='#e7e1ef', line_color = 'black', source = source)
            figure.rect(x = 'pos', y = -0.7, width = 'width', height = 1, fill_color='#e7e1ef', line_color = 'black', source = source)

        # MONITORS and INSTRUMENTS
        idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD) if ('MONITOR' in elem) or ('INSTRUMENT' in elem)])
        if len(idx) > 0:
            source = plots.create_Columndatasource(['pos', 'width', 'name'], [pos[idx], lengths[idx], np.array(twiss.NAME)[idx]])
            figure.rect(x = 'pos', y = 0, width = 'width', height = 2, fill_color='gray', line_color = 'black', source = source)

        # horizontal line at zero
        source = plots.create_Columndatasource(['pos', 'name'], [[0, twiss.S.iloc[-1]], ['START', 'END']])
        figure.line('pos', 0., line_width=.5, line_color = 'black', source = source)  
        print('Elements for HTML plot created.')

    def plot_magnetic_cycle(infile, outfile, output):
        
        # definition of parameters to be shown when hovering the mouse over the data points
        tooltips = [("parameter", "$name"), ("element", "@name"), ("value [m]", "$y")]
        
        if output == 'file':
            # output to static HTML file
            output_file(outfile, mode="inline")
        elif output == 'inline':
            output_notebook()
        
        # load the datasource
        data = pd.read_pickle(infile)
        
        source = ColumnDataSource(data)

        # calculate plot limits based on data range
        ymax = np.floor(np.max(data['B'])*10+1)/10
        factor = np.max(data['p'])/np.max(data['B'])

        # create a new plot with a title and axis labels
        f = figure(title="", x_axis_label='Cycle time [ms]', y_axis_label='Magnetic field [T]', width=400, height=250, x_range=Range1d(0, data['t'].iloc[-1], bounds="auto"), y_range=Range1d(0, ymax), tools="box_zoom, pan, reset", active_drag = 'box_zoom')

        f.axis.major_label_text_font = 'times'
        f.axis.axis_label_text_font = 'times'
        f.axis.axis_label_text_font_style = 'normal'
        f.outline_line_color = 'black'
        f.sizing_mode = 'scale_width'

        f.line('t', 'B', source=source, line_width=1.5, line_color = 'firebrick')

        # Setting the second y axis range name and range
        f.extra_y_ranges = {"p": Range1d(0, ymax * factor)}

        # Adding the second y axis to the plot.  
        f.add_layout(LinearAxis(y_range_name="p", axis_label='Momentum [GeV/c]', axis_label_text_font = 'times', axis_label_text_font_style = 'normal', major_label_text_font = 'times'), 'right')
        dx = f.line('t', 'p', source=source, line_width=1.5, line_color = 'darkblue', y_range_name="p")
        
        if output == 'file':
            save(f)
        elif output == 'inline':
            show(f)
